function formatDate(dateString) {
    const date = new Date(dateString);
    const options = { year: 'numeric', month: 'numeric', day: 'numeric' };
    return date.toLocaleDateString('en-US', options);
}

function createCard(name, description, picture_url, startDate, endDate, location) {
    const formattedStartDate = formatDate(startDate);
    const formattedEndDate = formatDate(endDate);
    return `
      <div class="col">
        <div class="card h-100 shadow">
            <img src="${picture_url}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">${formattedStartDate} - ${formattedEndDate}</small>
            </div>
        </div>
      </div>
    `;
}


function showAlert(message, type) {
    const alertPlaceholder = document.getElementById('liveAlertPlaceholder')
    const wrapper = document.createElement('div')
    wrapper.innerHTML = `
        <div class="alert alert-${type} alert-dismissible" role="alert">
            ${message}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    `;
    alertPlaceholder.append(wrapper)
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/";

    try {
        const response = await fetch(url);

        if (!response.ok) {
            showAlert(`Failed to fetch conferences. Status code : ${response.status}`, 'danger');
        } else {
            const data = await response.json();
            const row = document.querySelector('.row');
            row.innerHTML = '';

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const location = details.conference.location.name;
                    const picture_url = details.conference.location.picture_url;
                    const startDate = details.conference.starts;
                    const endDate = details.conference.ends;
                    const html = createCard(title, description, picture_url, startDate, endDate, location);
                    row.innerHTML += html;
                } else {
                    showAlert(`Failed to fetch details for a conference. Status code: ${detailResponse.status}`, 'warning');
                }
            }
            showAlert(`Successfully loaded conferences!`, 'success');
        }
    } catch (e) {
        console.error('An error occurred: ', e);
        showAlert('An error occurred while fetching conference data.', 'danger');
    }
});
