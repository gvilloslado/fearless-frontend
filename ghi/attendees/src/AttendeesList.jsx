import PropTypes from "prop-types";

function AttendeesList(props) {
  return (
    <>
      <h2>List of Attendees</h2>
      <table className="table table-striped table-bordered caption-top">
        <caption>Number of attendees: {props.attendees.length}</caption>
        <thead>
          <tr>
            <th className="col-5">Name</th>
            <th className="col-7">Conference</th>
          </tr>
        </thead>
        <tbody>
          {props.attendees.map((attendee) => {
            return (
              <tr key={attendee.href}>
                <td className="col-5">{attendee.name}</td>
                <td className="col-7">{attendee.conference}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </>
  );
}

AttendeesList.propTypes = {
  attendees: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      conference: PropTypes.string.isRequired,
    })
  ),
};

export default AttendeesList;
