window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/locations/";

  try {
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById("location");
      for (let location of data.locations) {
        const option = document.createElement("option");
        option.value = location.href.split("/").slice(-2, -1)[0]; // Extract the ID from the href
        option.innerHTML = location.name;
        selectTag.appendChild(option);
      }
    } else {
      console.error(
        "Failed to fetch locations:",
        response.status,
        response.statusText
      );
    }
  } catch (error) {
    console.error("Error fetching locations:", error);
  }

  const formTag = document.getElementById("create-conference-form");
  formTag.addEventListener("submit", async (event) => {
    event.preventDefault();
    const formData = new FormData(formTag);
    const formDataObj = Object.fromEntries(formData);
    formDataObj.location = parseInt(formDataObj.location, 10);
    formDataObj.max_presentations = parseInt(formDataObj.max_presentations, 10);
    formDataObj.max_attendees = parseInt(formDataObj.max_attendees, 10);
    const json = JSON.stringify(formDataObj);

    const conferenceUrl = "http://localhost:8000/api/conferences/";
    const fetchConfig = {
      method: "post",
      body: json,
      headers: {
        "Content-Type": "application/json",
      },
    };

    try {
      const response = await fetch(conferenceUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        await response.json();
        alert("Conference created successfully!");
      } else {
        const errorBody = await response.text();
        console.error(
          "Failed to create conference:",
          response.status,
          response.statusText,
          errorBody
        );
        alert("Failed to create conference. Please try again.");
      }
    } catch (error) {
      console.error("Error creating conference:", error);
      alert(
        "An error occurred while creating the conference. Please try again."
      );
    }
  });
});
