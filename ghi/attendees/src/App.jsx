import PropTypes from "prop-types";
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <Nav />
      <div className="container">
        <LocationForm />
        <AttendeesList attendees={props.attendees} />
      </div>
    </>
  );
}

App.propTypes = {
  attendees: PropTypes.arrayOf(
    PropTypes.shape({
      href: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      conference: PropTypes.string.isRequired,
    })
  ),
};

export default App;
